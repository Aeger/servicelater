package com.example.stc.servicenowoit;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import javax.net.ssl.HttpsURLConnection;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;



public class SNData extends AsyncTask<Integer, Void, Void> {

    private final WeakReference<View> ver;

    public SNData(View v){
        ver = new WeakReference<>(v);
    }

    public String getNEntriesFromTable(String tablename, int responsequant){
        String resp = "Test";

        if(responsequant < 0){

            return "TOO SMALL OF A PARAMETER";
        }

        try{
            URL sn = new URL("https://ucmercedtest.service-now.com/api/now/table/" + tablename + "?sysparm_view=" + Integer.toString(responsequant)); //Sorting can happen with +"&sysparm_query=active=true^ORDERBYnumber"
            HttpsURLConnection conn = (HttpsURLConnection) sn.openConnection();
            String auth = Base64.encodeToString(("sthd:HDspring$16").getBytes("UTF-8"), Base64.NO_WRAP);
            conn.setRequestProperty("Authorization", "Basic "+auth);
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;

            while((line=rd.readLine()) != null){
                Log.d("output", line);
            }

        } catch(Exception e){
            Log.d("Exception", "Exception is " + e);
            return "Exception occurred in getNEntriesFromTable: " + e;
        }

        return resp;

    }

    protected Void doInBackground(Integer... num){
        getNEntriesFromTable("incident", num[0]);

        return null;
    }

}
