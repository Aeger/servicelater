package com.example.stc.servicenowoit;


public class SNUser {
    private String UCMNetID;
    private String ServiceNowID;
    private String FullName;

    public SNUser(){
        UCMNetID = "";
        ServiceNowID = "";
        FullName = "";
    }


    public String getServiceNowID() {
        return ServiceNowID;
    }

    public void setServiceNowID(String serviceNowID) {
        ServiceNowID = serviceNowID;
    }

    public String getUCMNetID() {
        return UCMNetID;
    }

    public void setUCMNetID(String UCMNetID) {
        this.UCMNetID = UCMNetID;
    }

    public String getFullName(){
        return FullName;
    }

    public void setFullName(String FullName){
        this.FullName = FullName;
    }

}
