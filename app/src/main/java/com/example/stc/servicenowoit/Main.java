package com.example.stc.servicenowoit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateCheckboxes(findViewById(R.id.linearLayout2));
    }

    protected void populateCheckboxes(View v){
        String[] expressOptions = new String[]{"Wireless", "UCM Print", "Microsoft Office", "Matlab", "VPN", "Antivirus", "E-mail", "Other"};

        //TableLayout options = (TableLayout) findViewById(R.id.linearLayout2);
        TableLayout options = (TableLayout) v;
        TableRow row;

        for(int i = 0; i < expressOptions.length; i+=2) {
            row = new TableRow(getApplicationContext());
            CheckBox a = new CheckBox(this);
            CheckBox b = new CheckBox(this);

            a.setText(expressOptions[i]);

            row.addView(a);
            if(i+1 < expressOptions.length){
                row.addView(b);
            }

            options.addView(row);
        }


    }

    public void attemptConnection(View v){
        new SNData(v).execute(10);
    }

    public void fillNameFields(View v){
        String netID = obtainNetID(v);
        String result = querySNwNetID(netID); //or throw into hash map
        //SNUser employee; Put into another function to gain employee credentials for "assigned to"
        SNUser customer = new SNUser(); //Make a constructor to fill with NetID/result
        //Maybe make a new incident somewhere here to stick inc number with returned name

    }

    private String querySNwNetID(String netID) {
        //https://ucmercedtest.service-now.com/api/now/table/sys_user?GETNAMEFIELDLATER=netID will
        //return user
        return "";
    }

    public String obtainNetID(View v){
        getCardID(); //Maybe put here
        //Use upon CatCard scanner's activity to get the NetID
        //Assign NetID to SNUser object either here or on Service Now data obtainment
        //Maybe change return value to the NetID
        return "";
    }

    public String getCardID(){

        return "";
    }

}
